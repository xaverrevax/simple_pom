# -*- coding: utf8 -*-


def test_cases(number):
    return testCases[number]


testCases = [
    # [severity, description]
    ['Blocker', 'when user goes to main page, page should be loaded'],
    ['Blocker', 'set source text'],
    ['Blocker', 'rh selector'],
    ['Blocker', 'select target language'],
]